import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';

import { AppRoutingModule } from './app-routing.module';

import { AdminComponent } from './admin/admin.component';
import { AppComponent } from './app.component';
import { PostItemComponent } from './shared/components/post-item/post-item.component';
import { TimelineComponent } from './timeline/timeline.component';
import { ContentItemComponent } from './shared/components/content-item/content-item.component';

import { DataService } from './shared/services/data';

@NgModule({
  declarations: [
    AdminComponent,
    AppComponent,
    PostItemComponent,
    TimelineComponent,
    ContentItemComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    TextInputAutocompleteModule,
    TypeaheadModule.forRoot(),
    BrowserAnimationsModule,
  ],
  entryComponents: [
    ContentItemComponent,
  ],
  providers: [
    DataService,
  ],
  bootstrap: [
    AppComponent,
  ]
})

export class AppModule { }
