import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { DataService } from '../shared/services/data';

import { IPost } from '../shared/models/post';
import { IUser } from '../shared/models/user';

const names: string[] = [];

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0 }),
          animate('{{duration}}ms {{delay}}ms ease-out', style({ opacity: 1 }))
        ]),
        transition(':leave', [
          style({ opacity: 1 }),
          animate('500ms ease-out', style({ opacity: 0 }))
        ]),
      ]
    )
  ],
})

export class TimelineComponent implements OnInit {

  formPost = {} as IPost;
  allPosts: IPost[] = [];
  allUsers: IUser[] = [];

  constructor(
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.dataService.readAllUsers().subscribe((users) => {
      if (users) {
        users.map(user => {
          names.push(user.name);
        });
      }
    });
    this.dataService.readAllPosts().subscribe((posts) => {
      if (posts) {
        this.allPosts = posts;
        this.allPosts.sort((a, b) => {
          return b.timestamp - a.timestamp;
        });
      }
    });
  }

  createPost() {
    if (!this.formPost.content) {
      return;
    }
    this.formPost.timestamp = + new Date();
    this.allPosts.push(this.formPost);
    this.allPosts.sort((a, b) => {
      return b.timestamp - a.timestamp;
    });
    this.dataService.setItemSubscribe('allPosts', this.allPosts);
    this.formPost = {} as IPost;
  }

  findChoices(searchText: string) {
    return names.filter(user => user.toLowerCase().includes(searchText.toLowerCase()));
  }

  getChoiceLabel(choice: string) {
    return `[@${choice}] `;
  }

  onDeletePost(event) {
    const index = this.allPosts.findIndex(p => p.timestamp === event.timestamp);
    if (index) {
      this.allPosts.splice(index, 1);
      this.dataService.setItemSubscribe('allPosts', this.allPosts);
    }
  }

}
