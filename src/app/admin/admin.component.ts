import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { DataService } from '../shared/services/data';

import { IUser } from '../shared/models/user';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})

export class AdminComponent implements OnInit {

  formUser = {} as IUser;
  allUsers: IUser[] = [];
  allRoles: string[];
  messages: string;

  constructor(
    private dataService: DataService,
  ) {
  }

  ngOnInit() {
    console.log('ngOnInit AdminComponent');
    this.dataService.readAllUsers().pipe(
      map((data) => {
        if (data) {
          this.updateRolesList(data);
        }
        return data;
      })
    ).subscribe((users) => {
      if (users) {
        this.allUsers = users;
      }
    });
  }

  createUser() {
    this.formUser.id = this.generateId();
    this.formUser.readonly = true;
    if (this.validateUser(this.formUser)) {
      this.allUsers.push(this.formUser);
      this.updateRolesList(this.allUsers);
      this.dataService.setItemSubscribe('allUsers', this.allUsers);
      this.formUser = {} as IUser;
    }
  }

  validateUser(user: IUser) {
    this.messages = '';
    if (!user.username) {
      this.messages += '<p>Username is mandatory.</p>';
    } else {
      const reg = /^[\w-_]+$/g;
      if (!reg.test(user.username)) {
        this.messages += '<p>Username is not valid (1 word, _, -).</p>';
      }
    }
    if (!user.name) {
      this.messages += '<p>Name is mandatory.</p>';
    }
    if (!user.phone) {
      this.messages += '<p>Phone is mandatory.</p>';
    } else {
      const exists = this.allUsers.find(x => x.phone === user.phone && x.id !== user.id);
      if (exists) {
        this.messages += '<p>That Phone already exists.</p>';
      }
    }
    if (!user.role) {
      this.messages += '<p>Role is mandatory.</p>';
    }
    if (this.messages) {
      return false;
    }
    return true;
  }

  hideMessages() {
    this.messages = '';
  }

  editUser(index: number) {
    this.allUsers[index].readonly = false;
  }

  saveUser(index: number) {
    if (this.validateUser(this.allUsers[index])) {
      this.allUsers[index].readonly = true;
      this.updateRolesList(this.allUsers);
      this.dataService.setItemSubscribe('allUsers', this.allUsers);
    }
  }

  deleteUser(index: number) {
    this.allUsers.splice(index, 1);
    this.dataService.setItemSubscribe('allUsers', this.allUsers);
  }

  generateId() {
    const amount = this.allUsers.length;
    if (amount) {
      return this.allUsers[amount - 1].id + 1;
    } else {
      return 1;
    }
  }

  updateRolesList(data: any[]) {
    const roles = [];
    data.map((user: IUser) => {
      roles.push(user.role);
    });
    this.allRoles = roles.filter(function (item, pos) {
      return roles.indexOf(item) === pos;
    });
  }

}
