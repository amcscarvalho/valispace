export interface IUser {
    id: number;
    name: string;
    phone: string;
    readonly: boolean;
    role: string;
    username: string;
}
