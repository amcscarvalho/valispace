export interface IPost {
    content: string;
    id: number;
    timestamp: number;
}
