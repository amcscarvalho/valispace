import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { LocalStorage } from '@ngx-pwa/local-storage';
import { IUser } from '../models/user';
import { Observable } from 'rxjs';

@Injectable()

export class DataService {

    constructor(
        protected localStorage: LocalStorage,
    ) {

    }

    setItemSubscribe(key: string, content: any) {
        this.localStorage.setItemSubscribe(key, content);
    }

    read(key: string) {
        return this.localStorage.getItem(key);
    }

    readAllUsers() {
        return this.localStorage.getItem('allUsers');
    }

    readAllPosts() {
        return this.localStorage.getItem('allPosts');
    }

}
