import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IPost } from '../../models/post';

@Component({
    selector: 'app-post-item',
    templateUrl: 'post-item.component.html',
})

export class PostItemComponent {

    @Input() post: IPost;
    @Output() broadcastDelete = new EventEmitter<any>();

    constructor(
    ) { }

    deleteMe() {
        this.broadcastDelete.emit(this.post);
    }

}
