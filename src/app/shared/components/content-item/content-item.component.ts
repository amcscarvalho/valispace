import { Component, OnInit, Input } from '@angular/core';

import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'app-content-item',
    templateUrl: 'content-item.component.html',
})

export class ContentItemComponent implements OnInit {

    body: SafeHtml;

    @Input() postContent: string;

    constructor(
        private _sanitizer: DomSanitizer,
    ) { }

    ngOnInit() {
        const regex = /(\[.*?\])/g;
        let payload = this.postContent;
        let matches;
        while ((matches = regex.exec(this.postContent)) !== null) {
            payload = payload.replace(matches[0], '<strong>' + matches[0] + '</strong>').replace('[', '').replace(']', '');
        }
        this.body = this._sanitizer.bypassSecurityTrustHtml(payload);
    }

}
