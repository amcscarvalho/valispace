import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { TimelineComponent } from './timeline/timeline.component';

const routes: Routes = [
    {
        path: '',
        component: TimelineComponent,
    }, {
        path: 'admin',
        component: AdminComponent,
    }, {
        path: '**',
        redirectTo: '',
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: false,
        })
    ],
    exports: [
        RouterModule,
    ]
})

export class AppRoutingModule { }
